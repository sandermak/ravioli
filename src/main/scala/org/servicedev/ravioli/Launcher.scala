/**
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli

import java.util.prefs.Preferences
import org.servicedev.ravioli.impl.graph.GraphFrame
import java.lang.{Exception, String}
import javax.swing.SwingUtilities
import org.servicedev.ravioli.model.base.{DefaultComponentModel, DefaultDependency, DefaultComponent}
import org.servicedev.ravioli.impl.graph.model.GraphModel
import org.servicedev.ravioli.model.ComponentModel
import scala.collection.JavaConversions._
import java.io.File
import scala.collection.mutable.ListBuffer

object Launcher extends App {

  val options = parseCommandLineArgs(args.toList, Map())
  
  if (options.contains('debug)) {
    println("Enabling debug mode")
    System.setProperty(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY, "true")
  }

  if (options.contains('help)) {
    help()
    sys.exit()
  }

  if (options.contains('clearPrefs))
    clearPrefs()

  var initialAction: (GraphFrame) => Unit = null

  if (options.contains('demo)) {
    val demoModel = createDemo(options('demo))
    initialAction = (app: GraphFrame) => app.loadModel(demoModel);
  }

  if (options.contains('manifest)) {
    val dir = new File(options('manifest))
    initialAction = (app: GraphFrame) => app.loadManifestBundleDependencies(dir)
  }

  if (options.contains('bytecode)) {
    val jarFile = new File(options('bytecode))
    initialAction = (app: GraphFrame) => app.loadJarFilePackageDependencies(jarFile)
  }

  if (options.contains('classes)) {
    val classesDir = new File(options('classes))
    initialAction = (app: GraphFrame) => app.loadClassFilePackageDependencies(classesDir)
  }

  val app: GraphFrame = new GraphFrame();
  app.setVisible(true)
  if (initialAction != null)
    initialAction(app)

  def parseCommandLineArgs(argList: List[String], args: Map[Symbol, String]): Map[Symbol, String] = {
    argList match {
      case Nil => args
      case ("-h" | "--help")    :: tail => parseCommandLineArgs(tail, args ++ Map('help -> null))
      case "--demo" :: demo     :: tail => parseCommandLineArgs(tail, args ++ Map('demo -> demo))
      case "--clearPrefs"       :: tail => parseCommandLineArgs(tail, args ++ Map('clearPrefs -> null))
      case "--debug"            :: tail => parseCommandLineArgs(tail, args ++ Map('debug -> null))
      case "--manifests" :: dir :: tail => parseCommandLineArgs(tail, args ++ Map('manifest -> dir))
      case "--jar" :: jar       :: tail => parseCommandLineArgs(tail, args ++ Map('bytecode -> jar))
      case "--classes":: dir    :: tail => parseCommandLineArgs(tail, args ++ Map('classes -> dir))
      case unknown              :: tail => println("Unknown option " + unknown); parseCommandLineArgs(tail, args)
    }
  }

  def help() {
    val aboutText: String = s"${getName()} is open source and licensed under LGPL.\n" +
      "Included libraries:\n" +
      getLibs().mkString("\n") + "\n" +
      s"For more info see ${getInfoUrl}\n"

    println(getName() + " " + getVersion() + "\n")
    println(aboutText)
  }

  def clearPrefs() {
    val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])
    prefs.removeNode()
  }

  def createDemo(demo: String): ComponentModel = {
    var componentModel = new DefaultComponentModel

    if (demo == "xander") {
      val a: DefaultComponent = new DefaultComponent("A")
      val b: DefaultComponent = new DefaultComponent("B")
      val c: DefaultComponent = new DefaultComponent("C")
      val d: DefaultComponent = new DefaultComponent("D")
      val e: DefaultComponent = new DefaultComponent("E Met een bijzonder lang label.")
      val aToB: DefaultDependency = new DefaultDependency(a, b)
      a.addOutgoingDependency(aToB)
      val aToC: DefaultDependency = new DefaultDependency(a, c)
      a.addOutgoingDependency(aToC)
      val bToD: DefaultDependency = new DefaultDependency(b, d)
      b.addOutgoingDependency(bToD)
      componentModel.addComponent(a)
      componentModel.addComponent(b)
      componentModel.addComponent(c)
      componentModel.addComponent(d)
      componentModel.addComponent(e)

      for (i <- 0 to 30) {
        val id: String = "A " + i
        val component: DefaultComponent = new DefaultComponent(id)
        val componentToC: DefaultDependency = new DefaultDependency(component, c)
        component.addOutgoingDependency(componentToC)
        componentModel.addComponent(component)
      }
    } else if (demo == "performance") {
      val a: DefaultComponent = new DefaultComponent("A")
      val b: DefaultComponent = new DefaultComponent("B")
      val c: DefaultComponent = new DefaultComponent("C")
      val d: DefaultComponent = new DefaultComponent("D")
      val aToB: DefaultDependency = new DefaultDependency(a, b)
      a.addOutgoingDependency(aToB)
      val aToC: DefaultDependency = new DefaultDependency(a, c)
      a.addOutgoingDependency(aToC)
      val bToD: DefaultDependency = new DefaultDependency(b, d)
      b.addOutgoingDependency(bToD)
      componentModel.addComponent(a)
      componentModel.addComponent(b)
      componentModel.addComponent(c)
      componentModel.addComponent(d)

      val aComponents : ListBuffer[DefaultComponent] = new ListBuffer[DefaultComponent]()
      for (i <- 0 to 50) {
        var id: String = "A " + i
        var component: DefaultComponent = new DefaultComponent(id)
        val componentToC: DefaultDependency = new DefaultDependency(component, c)
        component.addOutgoingDependency(componentToC)
        componentModel.addComponent(component)
        aComponents.add(component)
      }
      for (i <- 0 to 500) {
        val id: String = "B " + i
        val component: DefaultComponent = new DefaultComponent(id)
        val cToComponent: DefaultDependency = new DefaultDependency(c, component)
        c.addOutgoingDependency(cToComponent)
        for (i <- 0 to 50) {
	        val aComponent = aComponents.get(i)
	        val componentToA: DefaultDependency = new DefaultDependency(component, aComponent)
	        component.addOutgoingDependency(componentToA)
        }
        componentModel.addComponent(component)
      }
    }
    else if (demo == "peter") {
      val a = new DefaultComponent("A")
      val b = new DefaultComponent("B")
      val c = new DefaultComponent("C")
      val d = new DefaultComponent("D")
      val aToC = new DefaultDependency(a, c).addSelf()
      val bToD = new DefaultDependency(b, d).addSelf()
      val cToD = new DefaultDependency(c, d).addSelf()
      componentModel = new DefaultComponentModel(List(a, b, c, d))
    }
    else if (demo == "abcdef" || true) {
      val a = new DefaultComponent("A")
      val b = new DefaultComponent("B")
      val c = new DefaultComponent("C")
      val d = new DefaultComponent("D")
      val e = new DefaultComponent("E")
      val f = new DefaultComponent("F")
      new DefaultDependency(a, c).addSelf()
      new DefaultDependency(b, d).addSelf()
      new DefaultDependency(c, d).addSelf()
      new DefaultDependency(d, e).addSelf()
      new DefaultDependency(d, f).addSelf()
      new DefaultDependency(f, e).addSelf()
      componentModel = new DefaultComponentModel(List(a, b, c, d, e, f))

    }
    return componentModel
  }

  def getName(): String = "Ravioli"

  def getInfoUrl(): String = "https://bitbucket.org/uiterlix/ravioli."

  def getLibs(): Seq[String] = {
    List(
      "- JGraph, see https://github.com/jgraph/jgraphx",
      "- MigLayout, see http://www.miglayout.com/",
      "- Open Icon Library, see http://openiconlibrary.sourceforge.net/",
      "- SLF4J, see http://www.slf4j.org/",
      "- slf4j-osgi, see http://marellbox.marell.se/bundles/slf4j-osgi/index.html"
    ) ++
      deploymentPackageSpecificLibs
  }

  def deploymentPackageSpecificLibs: Seq[String] = {
    try {
      Class.forName("ch.qos.logback.classic.Logger")
      // If it gets here, logback is included
      List("- Logback, see http://logback.qos.ch/")
    }
    catch {
      case e: ClassNotFoundException => List()
    }
  }

  def getExtendedName(): String = "Ravioli - no spaghetti"

  def getVersion(): String = {
    // Call version method of generated Version class with reflection, so IDE can still compile this project if Version class not yet generated.
    try {
      val className = this.getClass().getName()
      val mainPackage = className.take(className.lastIndexOf("."))
      val versionClass = Class.forName(mainPackage + ".Version")
      val versionMethod = versionClass.getMethod("getVersion")
      var version = versionMethod.invoke(null).asInstanceOf[String]
      if (! version.matches("\\d.*")) {
        version += " version"
      }
      return version;
    }
    catch {
      case e:Exception => println(e); return "unknown version"
    }
  }

}
