/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import java.util.ArrayList
import java.util.List
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * A label converter that converts label based on (regex, replacement) pairs.
 */
class RegexBasedLabelConverter extends LabelConverter {

  private var patterns: List[Pattern] = new ArrayList[Pattern]
  private var replacements: List[String] = new ArrayList[String]

  def setRegexReplacements(regexReplacements: List[String]) {
    patterns.clear
    replacements.clear
    var i: Int = 0
    while (i < regexReplacements.size) {
      {
        patterns.add(Pattern.compile(regexReplacements.get(i)))
        replacements.add(regexReplacements.get(i + 1))
      }
      i += 2
    }
  }

  def convert(label: String): String = convert(label, false)

  private def convert(label: String, escapeIllegalDollarExpressions: Boolean): String = {
    for (i <- 0 until patterns.size) {
      val m: Matcher = patterns.get(i).matcher(label)
      if (m.matches) {
        val buffer: StringBuffer = new StringBuffer
        val replacement: String = if (escapeIllegalDollarExpressions) escapeGroupRefsLargerThen(m.groupCount, replacements.get(i)) else replacements.get(i)
        try {
          m.appendReplacement(buffer, replacement)
        }
        catch {
          case noSuchGroup: IndexOutOfBoundsException => {
            assert(escapeIllegalDollarExpressions == false)
            return convert(label, true)
          }
          case incorrectDollar: IllegalArgumentException => {
            assert(escapeIllegalDollarExpressions == false)
            return convert(label, true)
          }
        }
        m.appendTail(buffer)
        return buffer.toString
      }
    }
    return label
  }

  private def escapeGroupRefsLargerThen(groupCount: Int, string: String): String = {
    val m: Matcher = Pattern.compile("\\$\\d*").matcher(string)
    val buffer: StringBuffer = new StringBuffer
    while (m.find) {
      var index: Int = -1
      if (m.group.length > 1) index = Integer.parseInt(m.group.substring(1))
      if (index >= 0 && index <= groupCount) {
        m.appendReplacement(buffer, "\\$" + index)
      }
      else if (index < 0) {
        m.appendReplacement(buffer, "\\\\\\$")
      }
      else {
        m.appendReplacement(buffer, "\\\\\\$" + index)
      }
    }
    m.appendTail(buffer)
    return buffer.toString
  }
}
