/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph.model

import org.servicedev.ravioli.impl.graph.model.GraphModelChangeEvent.ChangeTypeEnum.EventType

/**
 * Change event describing a graph model change.
 */

class GraphModelChangeEvent(eventType: EventType, graphComponent: GraphComponent, graphDependency: GraphDependency) {

  def this(eventType: EventType, component: GraphComponent) = this(eventType, component, null)

  def this(eventType: EventType, dependency: GraphDependency) = this(eventType, null, dependency)

  def this(eventType: EventType) = this(eventType, null, null)

  /**
   * Returns the type of change.
   * @return
   */
  def getType: EventType = eventType

  /**
   * Returns the {@link GraphComponent} that was the actual subject of the change event.
   * @return
   */
  def getGraphComponent: GraphComponent = graphComponent

  /**
   * Returns the {@link GraphDependency} that was the actual subject of the change event.
   * @return
   */
  def getGraphDependency: GraphDependency = graphDependency
}

object GraphModelChangeEvent {

  object ChangeTypeEnum extends Enumeration {
    type EventType = Value
    val GRAPH_CHANGE, GRAPH_RESET = Value
  }

}
