/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.adapter

import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.Component
import scala.collection.mutable.ListBuffer
import org.servicedev.ravioli.model.bundle.BundleComponentModelPresentationAdapter
import scala.collection.immutable.List
import org.servicedev.ravioli.model.dm.DependencyManagerComponentModelPresentationAdapter

/**
 * Factory responsible for providing a PresentationAdapter for a given ComponentModel.
 * Implemented as singleton. Presentation adapters should register themselves with the factory though
 * the register/unregister methods.
 */
object PresentationAdapterFactory {
  
  var presentationAdapters = List[PresentationAdapter]()
  
  // static adding of 'known' presentation adapters
  {
    register(new BundleComponentModelPresentationAdapter())
    register(new DependencyManagerComponentModelPresentationAdapter())
  }

  /**
   * Obtain a presentation adapter for the given ComponentModel
   */
  def getPresentationAdapter(componentModel : ComponentModel) : Option[PresentationAdapter] = {
	val applicableAdapters = presentationAdapters.filter(presentationAdapter => presentationAdapter.isAdapterFor(componentModel))
	if (applicableAdapters.size > 0) {
	  Some(applicableAdapters(0))
	} else {
      None
	}
  }
  
  /**
   * Register a PresentationAdapter
   */
  def register(presentationAdapter : PresentationAdapter) = {
    presentationAdapters = presentationAdapters :+ presentationAdapter
  }

  /**
   * Unregister a PresentationAdapter
   */
  def unregister(presentationAdapter : PresentationAdapter) = {
    presentationAdapters = presentationAdapters diff List(presentationAdapter)
  }
}

/**
 * Presentation adapter which provides presentation 'hints' though providing presentation types
 * for both Dependency and Component. These presentation types can at UI level be mapped to concrete
 * rendering attributes.
 */
abstract trait PresentationAdapter {
  
  /**
   * Returns whether the presentation adapter is applicable for the given ComponentModel.
   */
  def isAdapterFor(componentModel : ComponentModel) : Boolean

  /**
   * Returns the presentation types that are used for components
   */
  def componentPresentationTypes: Seq[String]

  /**
   * Returns the presentation types that are used for dependencies
   */
  def dependencyPresentationTypes: Seq[String]

  /**
   * Returns the presentation type for the given Dependency.
   */
  def getPresentationType(dependency : Dependency) : Option[String]
  
  /**
   * Returns the presentation type for the given Component.
   */
  def getPresentationType(component : Component) : Option[String]
  
}