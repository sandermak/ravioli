/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.dm

import java.util.List
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Component
import org.osgi.framework.BundleContext
import org.apache.felix.dm.DependencyManager
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import org.apache.felix.dm.ComponentDeclaration
import org.osgi.framework.ServiceReference
import org.apache.felix.dm.ServiceDependency
import scala.reflect.runtime.universe._
import scala.collection.mutable.Map
import org.osgi.framework.ServiceRegistration
import java.util.UUID
import java.util.Collections
import org.apache.felix.dm.ComponentDependencyDeclaration
import org.apache.felix.dm.impl.FilterService

/**
 * Component model for dependency manager components in combination with OSGi services. Two types of outgoing
 * dependencies are created. 1. a dependency to the OSGi service that is being published by the DM component.
 * 2. dependencies for each service dependency of the dependency manager component. These dependencies are also
 * dependencies to OSGi services.
 */
class DependencyManagerComponentModel(bundleContext : BundleContext) extends ComponentModel {
  
  val serviceIdToServiceReferenceComponent = Map[Long, ServiceReferenceComponent]()
  var components : List[Component] = null
  var propertyKeys : scala.collection.immutable.List[String] = scala.collection.immutable.List[String]()
  
  {
    propertyKeys = propertyKeys :+ "type"
    propertyKeys = propertyKeys :+ "state"
    Some(propertyKeys)
  }
  
  def getComponents(): List[Component] = {
    if (components == null) {
	    var newComponents = ListBuffer[Component]()
		for (dependencyManager <- DependencyManager.getDependencyManagers()) {
		  for (component <- dependencyManager.asInstanceOf[DependencyManager].getComponents()) {
		    val dmComponent = new DMComponent(component.asInstanceOf[org.apache.felix.dm.Component])
		    val dependencies = component.asInstanceOf[org.apache.felix.dm.Component].getDependencies()
		    for (dependency <- dependencies) {
		      val dependencyComponent = new DependencyComponent(dependency.asInstanceOf[org.apache.felix.dm.Dependency])
		      newComponents += dependencyComponent
		      val dependencyDependency = new DMDependencyDependency(dmComponent, dependencyComponent)
		      dmComponent.addOutgoingDependency(dependencyDependency)
		      
		      if (dependency.asInstanceOf[org.apache.felix.dm.Dependency].isAvailable()) {
		    	  // only process service dependencies for now
			      if (dependency.isInstanceOf[ServiceDependency]) {
			        for (serviceId <- getWiredServiceIdentifiers(dependency.asInstanceOf[ServiceDependency])) {
			          val dmServiceDependency = new DMServiceDependency(this, dmComponent, serviceId, dependency.asInstanceOf[ServiceDependency])
			          dependencyComponent.addOutgoingDependency(dmServiceDependency)
			        }
			      }
		      } 
		    }
		    val serviceId = getServiceId(component.asInstanceOf[org.apache.felix.dm.Component])
		    if (serviceId.isDefined) {
		    	val dmImplDependency = new DMImplementationDependency(this, dmComponent, serviceId.get)
		    	dmComponent.addOutgoingDependency(dmImplDependency)
		    }
		    newComponents += dmComponent
		  }
		}
	    // add all services found in the service registry
	    val serviceReferences = bundleContext.getAllServiceReferences(null, null)
	    for (serviceReference <- serviceReferences) {
	      val serviceReferenceComponent = new ServiceReferenceComponent(serviceReference)
	      val serviceId = serviceReferenceComponent.getServiceId
	      newComponents += serviceReferenceComponent
	      serviceIdToServiceReferenceComponent(serviceId) = serviceReferenceComponent
	    }
	    components = newComponents
    }
    components
  }

  /**
   * Obtain the service identifier for the OSGi service published by this component.
   */
  def getServiceId(component : org.apache.felix.dm.Component) : Option[Long] = {
    val serviceRegistration = component.getServiceRegistration()
    if (serviceRegistration != null) {
      val serviceReference = serviceRegistration.getReference()
      if (serviceReference != null) {
        return Some(serviceReference.getProperty("service.id").asInstanceOf[Long])
      }
    }
    None
  }
  
  /**
   * Obtain the service identifiers for the services wired to this component.
   */
  def getWiredServiceIdentifiers(dependency : ServiceDependency) : List[Long] = {
    val serviceIds = ListBuffer[Long]()
    // m_tracker -> tracked -> tracked
    try {
	    val trackerField = dependency.getClass().getDeclaredField("m_tracker")
	    trackerField.setAccessible(true)
	    val tracker = trackerField.get(dependency)
	    if (tracker != null) {
	    	val trackedField = tracker.getClass().getDeclaredField("tracked")
	    	trackedField.setAccessible(true)
	    	val tracked = trackedField.get(tracker)
	    	if (tracked != null) {
	    	  val trackedMapField = tracked.getClass().getSuperclass().getDeclaredField("tracked")
	    	  trackedMapField.setAccessible(true)
	    	  val trackedMap = trackedMapField.get(tracked).asInstanceOf[java.util.Map[Object, Object]]
	    	  trackedMap.foreach { case (key, value) => 
	    	    val serviceId = key.asInstanceOf[ServiceReference].getProperty("service.id").asInstanceOf[Long]
	    	    serviceIds += serviceId
	    	  }
	    	}
	    }
    } catch {
    	case e : Throwable => { println("Could not determine injected services for dependency " + dependency) } // ignore 
    }
    serviceIds
  }

  override def getCreationResult(): String = null

  override def explainDependencies(dependencies: Seq[Dependency]): String = null
  
  override def getPropertyKeys() : Option[scala.collection.immutable.List[String]] = {
    Some(propertyKeys)
  }
}

/**
 * Component representing a dependency manager component.
 */
class DMComponent(val dmComponent: org.apache.felix.dm.Component) extends Component {
  
  var identifier = UUID.randomUUID().toString()
  val dependencies = ListBuffer[Dependency]()
  var label : String = null
  var properties : scala.collection.immutable.Map[String, String] = null
  
  override def getIdentifier() : String = {
    identifier
  }

  override def getLabel() : String = {
    // caching label since getName takes all service properties into account which is a costly operation.
    if (label == null) {
      label = dmComponent.asInstanceOf[ComponentDeclaration].getName()
    }
    label
  }
  
  override def getOutgoingDependencies() : List[Dependency] = {
    dependencies
  }
  
  def addOutgoingDependency(dependency : Dependency) {
    dependencies += dependency
  }
  
  override def getProperties() : Option[scala.collection.immutable.Map[String, String]] = {
    // cache properties once obtained
    if (properties == null) {
      properties = scala.collection.immutable.Map[String, String]()
      if (dmComponent.isInstanceOf[ComponentDeclaration]) {
        val state = dmComponent.asInstanceOf[ComponentDeclaration].getState()
        // 0 = unregistered, 1 = registered
        if (state == 0) {
          properties = properties + ("state" -> "unregistered")
        } else if (state == 1) {
          properties = properties + ("state" -> "registered")
        }
      }
      // temporary try/catch due to current dm bug (https://issues.apache.org/jira/browse/FELIX-4304)
      try {
        val componentProperties = dmComponent.getServiceProperties()
        if (componentProperties != null) {
          componentProperties.foreach { case (key, value) => {
            properties = properties + (key.toString -> DependencyManagerComponentModel.toPresentableValue(value))
          }}
        }
      } catch {
        case ex: ClassCastException => // Ignore
      }
      val serviceRegistration = dmComponent.getServiceRegistration()
      if (serviceRegistration != null) {
        val serviceReference = serviceRegistration.getReference()
        serviceReference.getPropertyKeys().foreach { key =>
          val value = serviceReference.getProperty(key)
          properties = properties + (key -> DependencyManagerComponentModel.toPresentableValue(value))
        }
      }
      if (dmComponent.getService() != null) {
    	  properties = properties + ("implementation" -> dmComponent.getService().toString )
      }
      properties = properties + ("type" -> "component")
      // additional implementation property for filter services
      if (dmComponent.getClass().getSuperclass().getName() == "org.apache.felix.dm.impl.FilterService") {
	    val serviceImplField = dmComponent.getClass().getSuperclass().getDeclaredField("m_serviceImpl")
	    serviceImplField.setAccessible(true)
	    val serviceImpl = serviceImplField.get(dmComponent)
	    properties = properties + ("implementation" -> serviceImpl.toString())
      }
    }
    Some(properties)
  }  
  
}

/**
 * Component based on an OSGi service reference.
 */
class ServiceReferenceComponent(val serviceReference : ServiceReference) extends Component {
  
  var identifier = UUID.randomUUID().toString()
  var label : String = null
  var properties : scala.collection.immutable.Map[String, String] = null
  
  override def getIdentifier() : String = {
    identifier
  }
  
  override def getLabel() : String = {
    if (label == null) {
    	label = serviceReference.toString()
    }
    label
  }
  
  override def getOutgoingDependencies : List[Dependency] = {
    var outgoingDependencies = ListBuffer[Dependency]()
    outgoingDependencies
  }
  
  def getServiceId() : Long = {
    serviceReference.getProperty("service.id").asInstanceOf[Long]
  }
  
  override def getProperties() : Option[scala.collection.immutable.Map[String, String]] = {
    // cache properties once obtained
    if (properties == null) {
	    properties = scala.collection.immutable.Map[String, String]()
	    serviceReference.getPropertyKeys().foreach { key =>
	      val value = serviceReference.getProperty(key)
	      properties = properties + (key -> DependencyManagerComponentModel.toPresentableValue(value))
	    }
	    properties = properties + ("type" -> "service")
  	}
    Some(properties)
  }
  
}

class DependencyComponent(val dmDependency : org.apache.felix.dm.Dependency) extends Component {
  
  var identifier = UUID.randomUUID().toString()
  val dependencies = ListBuffer[Dependency]()
  var properties : scala.collection.immutable.Map[String, String] = null
  
  override def getIdentifier() : String = {
    identifier
  }
  
  override def getLabel() : String = {
    if (dmDependency.isInstanceOf[ComponentDependencyDeclaration]) {
    	return dmDependency.asInstanceOf[ComponentDependencyDeclaration].getName() 
    }
    return dmDependency.toString()
  }
  
  override def getOutgoingDependencies : List[Dependency] = {
    dependencies
  }
  
  def addOutgoingDependency(dependency : Dependency) = {
    dependencies += dependency
  }
  
  def isAvailable() = {
    dmDependency.isAvailable()
  }
  
  def isRequired() = {
    dmDependency.isRequired()
  }
  
  override def getProperties() : Option[scala.collection.immutable.Map[String, String]] = {
    // cache properties once obtained
    if (properties == null) {
	    properties = scala.collection.immutable.Map[String, String]()
	    properties = properties + ("class" -> dmDependency.getClass().getName())
	    properties = properties + ("toString" -> dmDependency.toString())
	    properties = properties + ("type" -> "dependency")
	    if (isRequired) {
	      if (isAvailable) {
	        properties = properties + ("state" -> "required-available")
	      } else {
	        properties = properties + ("state" -> "required-unavailable")
	      }
	    } else {
	      if (isAvailable) {
	        properties = properties + ("state" -> "optional-available")
	      } else {
	        properties = properties + ("state" -> "optional-unavailable")
	      }
	    }
	    
    }
    Some(properties)
  }
  
}

class DMDependencyDependency(component : DMComponent, dependencyComponent : DependencyComponent) extends Dependency {
  
  def getSourceComponent() : Component = {
    component
  }
  
  def getTargetComponent() : DependencyComponent = {
    dependencyComponent
  }
}

/**
 * Dependency illustrating: component --requires--> service
 */
class DMServiceDependency(model : DependencyManagerComponentModel, component : Component, serviceId : Long, dmDependency : org.apache.felix.dm.Dependency) extends Dependency {
  
  def getSourceComponent() : Component = {
    component
  }
  
  def getTargetComponent() : Component = {
    model.serviceIdToServiceReferenceComponent(serviceId)
  }
}

/**
 * Dependency illustrating: component --publishes--> service
 */
class DMImplementationDependency(model : DependencyManagerComponentModel, component : Component, serviceId : Long) extends Dependency {

  def getSourceComponent() : Component = {
    component
  }
  
  def getTargetComponent() : Component = {
    model.serviceIdToServiceReferenceComponent(serviceId)
  }
  
}

object DependencyManagerComponentModel {
    def toPresentableValue(value : Any) : String = {
      var propertyValue : String = null
      if (value.isInstanceOf[Array[String]]) {
        propertyValue = value.asInstanceOf[Array[String]].deep.mkString(",")
      } else {
        propertyValue = value.toString()
      }
      propertyValue
  }
}

