/**
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import org.servicedev.ravioli.model.{Component, ComponentModel}
import scala.collection.mutable
import org.servicedev.ravioli.model.base.{DefaultDependency, DefaultComponent}
import scala.collection.JavaConversions._

class PackageComponentModel(analyzer: ByteCodeAnalyzer) extends ComponentModel {

  val components = createModel()

  def getComponents = components

  def createModel(): List[Component] = {
    val packageComponents = new mutable.HashMap[String, DefaultComponent]

    analyzer.allPackages.foreach { pckg: String =>
      packageComponents.put(pckg, new DefaultComponent(pckg))
    }

    analyzer.allPackages.foreach { pckg: String =>
      val currentComponent = packageComponents(pckg)
      analyzer.dependentPackages(pckg).foreach { dependent: String =>
        val targetComponent = packageComponents.get(dependent)
        if (targetComponent.isDefined) {
        val dep = new DefaultDependency(currentComponent, targetComponent.get)
        currentComponent.addOutgoingDependency(dep)
        }
        else {
          println("unsatisfied dep: " + dependent)
        }
      }
    }
    packageComponents.values.toList
  }
}
