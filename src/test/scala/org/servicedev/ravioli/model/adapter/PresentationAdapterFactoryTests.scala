/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.model.adapter

import org.scalatest.{Matchers, FunSuite}
import org.scalatest.mock.MockitoSugar
import org.servicedev.ravioli.model.base.{DefaultDependency, DefaultComponentModel, DefaultComponent}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import org.servicedev.ravioli.model.bundle.PackageDependency
import org.servicedev.ravioli.model.adapter.PresentationAdapter
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.Component
import org.servicedev.ravioli.model.adapter.PresentationAdapterFactory
import org.servicedev.ravioli.model.bundle.BundleDependency
import org.servicedev.ravioli.model.bundle.BundleComponentModel


class PresentationAdapterFactoryTests extends FunSuite with Matchers with MockitoSugar {

  test("register and unregister presentation adapter with presentation adapter factory") {
    val adapterA = new TestPresentationAdapterA()
    val adapterB = new TestPresentationAdapterB()
	PresentationAdapterFactory.register(adapterA)
	PresentationAdapterFactory.register(adapterB)
	val componentModel = new DefaultComponentModel()
	val componentModelA = new ComponentModelA()
	val componentModelB = new ComponentModelB()
	PresentationAdapterFactory.getPresentationAdapter(componentModel).isDefined shouldEqual false
	PresentationAdapterFactory.getPresentationAdapter(componentModelA).get shouldEqual adapterA
	PresentationAdapterFactory.getPresentationAdapter(componentModelB).get shouldEqual adapterB
	
	PresentationAdapterFactory.unregister(adapterA)
	PresentationAdapterFactory.unregister(adapterB)
	PresentationAdapterFactory.getPresentationAdapter(componentModelA).isDefined shouldEqual false
	PresentationAdapterFactory.getPresentationAdapter(componentModelB).isDefined shouldEqual false
  }
  
  test("presentation type for bundle compomponent model dependencies") {
    val bundleComponentModel = mock[BundleComponentModel]
    val packageDependency = mock[PackageDependency]
    val bundleDependency = mock[BundleDependency]
    PresentationAdapterFactory.getPresentationAdapter(bundleComponentModel).get.getPresentationType(packageDependency).get shouldEqual "package"
    PresentationAdapterFactory.getPresentationAdapter(bundleComponentModel).get.getPresentationType(bundleDependency).get shouldEqual "bundle"
  }
  
}

class ComponentModelA extends DefaultComponentModel {
  
}

class ComponentModelB extends DefaultComponentModel {
  
}

class TestPresentationAdapterA extends PresentationAdapter {
  def isAdapterFor(componentModel : ComponentModel) : Boolean = {
    componentModel.isInstanceOf[ComponentModelA]
  }

  def componentPresentationTypes = List()

  def dependencyPresentationTypes = List()

  def getPresentationType(dependency : Dependency) : Option[String] = {
    None
  }
  def getPresentationType(component : Component) : Option[String] = {
    None
  }
}

class TestPresentationAdapterB extends PresentationAdapter {
  def isAdapterFor(componentModel : ComponentModel) : Boolean = {
    componentModel.isInstanceOf[ComponentModelB]
  }

  def componentPresentationTypes = List()

  def dependencyPresentationTypes = List()

  def getPresentationType(dependency : Dependency) : Option[String] = {
    None
  }
  def getPresentationType(component : Component) : Option[String] = {
    None
  }  
}