/**
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.bundle

import org.scalatest.Matchers
import org.scalatest.FunSuite

class ManifestTest extends FunSuite with Matchers {

  test("parse simple import-package") {

    val packages = Manifest.parsePackageHeader("javax.swing,javax.swing.border,javax.swing.event")

    packages should equal(List("javax.swing", "javax.swing.border", "javax.swing.event"))
  }

  test("parse import-package with version declarations") {

    val packages = Manifest.parsePackageHeader("org.osgi.framework;version=\"[1.3,2)\",org.osgi.service.log;version=\"[1.3,2)\"")

    packages should equal(List("org.osgi.framework", "org.osgi.service.log"))
  }

  test("parse export-package with spaces") {
    val packages = Manifest.parsePackageHeader("javax.swing, javax.swing.border, javax.swing.event")

    packages should equal(List("javax.swing", "javax.swing.border", "javax.swing.event"))
  }

  test("parse empty import-package") {
    val packages = Manifest.parsePackageHeader("")

    packages should have size 0
  }

  test("BSN with singleton directive should be parsed correctly") {
    val (bsn, singleton) = Manifest.parseBundleSymbolicNameHeader("com.acme.bundle;singleton:=true")

    bsn shouldEqual "com.acme.bundle"
    singleton shouldBe true
  }

  test("complex BSN without directive should be parsed correctly") {
    val (bsn, singleton) = Manifest.parseBundleSymbolicNameHeader("c_0_m.ac-me.bundle.294587")

    bsn shouldEqual "c_0_m.ac-me.bundle.294587"
    singleton shouldBe false
  }

  test("no BSN should parse correctly") {
    val (bsn, singleton) = Manifest.parseBundleSymbolicNameHeader(null)

    bsn shouldEqual null
    singleton shouldBe false
  }

  test("require-bundle header can contain several bundles") {
    val requireBundleValue = "org.eclipse.ui;bundle-version=\"3.6.0\",org.eclipse.gmf.runtime.common.core;bundle-version=\"1.4.0\",org.eclipse.ui.ide;bundle-version=\"3.7.0\""

    val requiredBundles = Manifest.parsePackageHeader(requireBundleValue)

    requiredBundles should have size 3
    requiredBundles should contain ("org.eclipse.ui")
    requiredBundles should contain ("org.eclipse.gmf.runtime.common.core")
    requiredBundles should contain ("org.eclipse.ui.ide")
  }
}
